<object data="https://gitlab.com/corsys/machine-setup/-/raw/main/setup.pdf" type="application/pdf">
    <embed src="https://gitlab.com/corsys/machine-setup/-/raw/main/setup.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="https://gitlab.com/corsys/machine-setup/-/raw/main/setup.pdf">Download PDF</a>.</p>
    </embed>
</object>
