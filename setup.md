This document describes the setup of a Cascade Lake server machine with
Debian 11 for use in profiling and analysis of applications running on
heterogeneous memory architectures.

# Machine Setup

-   Enable LLC Prefetcher in the BIOS\
    `System BIOS`  `Processor Settings`  `LLC Prefetch`

-   Enable RAID 1 for HDDs (if applicable):

    -   `Device Settings`  `RAID Controller`  `Configure`
         `Create Virtual Disk`

    -   Select RAID Level: `RAID1`

    -   Select Physical Disks

    -   Check All, Apply Changes

    -   Name the virtual disk `HDD`.

    -   Create Virtual Disk

    -   Confirm `Yes`.

# Operating System

Install Debian 11 on the machine using a NetInstall USB drive.

-   Step through the graphical install until you get to "Configure the
    Network". Select the `eno3` interface.

-   Set the domain name to `eecs.utk.edu`.

-   Generate a root password and keep it somewhere secure.

-   Set up a user.

-   Partition Disks:

    -   Select "Manual".

    -   Clear all existing partitions.

    -   Create an EFI partition:

        -   Make this partition `10 GB` at the beginning of the disk.

        -   (We use a larger EFI partition to store custom kernels.)

    -   Either create on partition for the root filesystem (`/`), or
        create one for root and one for `/home`. This is up to you and
        you may choose to put one of those partitions on a different
        drive if one is available.

-   Continue through the installation until you get to "Software
    selection". Select only `SSH server` and\
    \* `standard system utilities`.

-   Complete the installation and boot into the new system.

# Users and Groups

-   Log in as `root`.

-   Install some basic software:

    ``` {.bash language="bash"}
    root$ apt install build-essential git sudo vim
    ```

-   Create users:

    ``` {.bash language="bash"}
    root$ adduser ian
    ```

-   Add users to the `sudo` group:

    ``` {.bash language="bash"}
    root$ usermod -aG sudo ian
    ```

# Custom Kernel

This process will install a custom kernel and enable eBPF tooling.

-   Install dependencies:

    ``` {.bash language="bash"}
    ian$ sudo apt install autoconf bc bison cpio fakeroot flex git kmod libelf-dev libncurses-dev libssl-dev xz-utils rsync dwarves
    ```

-   Clone the custom kernel:

    ``` {.bash language="bash"}
    ian$ git clone https://gitlab.com/corsys/corsys-linux
    ```

-   Check out a version:

    ``` {.bash language="bash"}
    ian$ cd corsys-linux && git checkout corsys-5.14
    ```

-   Check/edit the config file:

    ``` {.bash language="bash"}
    ian$ vim .config
    ```

    Ensure these options are set to `y` for BPF:

    -   `CONFIG_BPF`

    -   `CONFIG_BPF_SYSCALL`

    -   `CONFIG_BPF_JIT`

    -   `CONFIG_BPF_EVENTS`

    -   `CONFIG_DEBUG_INFO`

    -   `CONFIG_DEBUG_INFO_BTF`

    Custom lab kernel options (set to `y` as needed):

    -   `CONFIG_CGROUP_PER_NODE_MAX`: limits the capacity of data that
        can be placed in the upper tier of memory for processes in a
        cgroup.

    -   `CONFIG_MADV_COMPRESS`: adds an option to `madvise` that
        instructs the kernel to compress the memory range.

    -   `CONFIG_OBJECT_MAP`: allows access to information about
        arbitrary page ranges in a simple `proc` file interface.

-   Build and install the kernel:

    ``` {.bash language="bash"}
    ian$ make -j$(nproc) 2>&1 > out.txt # check for errors
    ian$ sudo make modules_install
    ian$ sudo make headers_install
    ian$ sudo make install
    ian$ sudo make -j $(nproc) -C tools/ perf_install prefix=/usr/
    ```

-   Reboot (you may need to select the new kernel in the `grub` menu)

    ``` {.bash language="bash"}
    ian$ sudo reboot
    ```

# Essential Software

-   Replace `mawk` with `gawk`

    ``` {.bash language="bash"}
    ian$ sudo apt install gawk
    ```

-   Install `perfmon`

    ``` {.bash language="bash"}
    ian$ sudo apt install libpfm4-dev
    ```

-   LLVM and Clang

    ``` {.bash language="bash"}
    ian$ mkdir -p ~/src && cd ~/src
    ian$ git clone https://github.com/llvm/llvm-project llvm && cd llvm
    ian$ # optionally, check out a version
    ian$ git checkout release/13.x
    ian$ mkdir build && cd build
    ian$ cmake ../llvm -DLLVM_ENABLE_PROJECTS='clang' -DCMAKE_BUILD_TYPE='RelWithDebInfo'
    ian$ make -j$(nproc) && sudo make install
    ```

# AEP NUMA Mode

This describes how to set up the Intel Optane PMM as a separate numa
device, a.k.a. AppDirect mode.

-   Install `ndctl` and `ipmctl`:

    ``` {.bash language="bash"}
    ian$ sudo apt install autogen libnuma-dev numactl libtool libkmod-dev libudev-dev uuid-dev libjson-c-dev libkeyutils-dev pkg-config asciidoctor libiniparser-dev ipmctl ndctl
    ```

-   Put all PMMs on socket 0 into AppDirect mode:

    ``` {.bash language="bash"}
    ian$ sudo ipmctl create -goal -socket 0 PersistentMemoryType=AppDirect
    ```

-   Reboot.

-   Migrate the device model:

    ``` {.bash language="bash"}
    ian$ sudo daxctl migrate-device-model
    ```

-   Reboot.

-   Create a namespace for the memory region:

    ``` {.bash language="bash"}
    ian$ sudo ndctl create-namespace --region region0 -m dax
    ```

-   Copy module config file:

    ``` {.bash language="bash"}
    ian$ sudo cp /usr/share/daxctl/daxctl.conf /etc/modprobe.d
    ```

-   Reboot.

-   Bind the namespace to NUMA:

    ``` {.bash language="bash"}
    ian$ sudo daxctl reconfigure-device --no-movable --mode=system-ram all
    ```

-   Check for correctness:

    ``` {.bash language="bash"}
    ian$ numastat -m
    ```

# Miscellaneous Tips

-   Disable Transparent Hugepages:

    ``` {.bash language="bash"}
    ian$ echo "never" | sudo tee /sys/kernel/mm/transparent_hugepage/enabled
    ```

-   Use `CONFIG_CGROUP_PER_NODE_MAX` to limit PID `12345` to 20GB in the upper tier:

    ``` {.bash language="bash"}
    ian$ echo "+cpuset" | sudo tee /sys/fs/cgroup/cgroup.subtree_control
    ian$ sudo rmdir /sys/fs/cgroup/1
    ian$ sudo mkdir /sys/fs/cgroup/1
    ian$ echo "21474836480" | sudo tee /sys/fs/cgroup/1/memory.node0_max
    ian$ echo "12345" | sudo tee /sys/fs/cgroup/1/cgroup.procs
    ```

# Optional Software

-   `zsh`

    ``` {.bash language="bash"}
    ian$ sudo apt install zsh
    ian$ chsh -s $(which zsh) ian
    ```

-   `tmux` and `htop`

    ``` {.bash language="bash"}
    ian$ sudo apt install tmux htop
    ```

-   `yed`

    ``` {.bash language="bash"}
    ian$ git clone https://github.com/kammerdienerb/yed && cd yed
    ian$ sudo ./install.sh
    ```

-   `mosh`

    ``` {.bash language="bash"}
    ian$ mkdir -p ~/src && cd ~/src
    ian$ git clone https://github.com/mobile-shell/mosh && cd mosh
    ian$ sudo apt install protobuf-compiler libprotobuf-dev libncurses-dev libssl-dev autogen pkg-config
    ian$ make -j$(nproc) && sudo make install
    ```
