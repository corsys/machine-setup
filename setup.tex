\documentclass{article}

\usepackage[margin=0.5in]{geometry}
\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=none,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}

\lstset{style=mystyle}

\begin{document}

This document describes the setup of a Cascade Lake server machine with Debian 11 for use in profiling and analysis of applications running on heterogeneous memory architectures.

\section{Machine Setup}
\begin{itemize}
    \item Enable LLC Prefetcher in the BIOS \\
    \verb|System BIOS| \textrightarrow ~\verb|Processor Settings| \textrightarrow ~\verb|LLC Prefetch|
    \item Enable RAID 1 for HDDs (if applicable):
        \begin{itemize}
            \item \verb|Device Settings| \textrightarrow ~\verb|RAID Controller| \textrightarrow ~\verb|Configure| \textrightarrow ~\verb|Create Virtual Disk|
            \item Select RAID Level: \verb|RAID1|
            \item Select Physical Disks
            \item Check All, Apply Changes
            \item Name the virtual disk \verb|HDD|.
            \item Create Virtual Disk
            \item Confirm \verb|Yes|.
        \end{itemize}

\end{itemize}
\section{Operating System}
    Install Debian 11 on the machine using a NetInstall USB drive.
    \begin{itemize}

        \item Step through the graphical install until you get to ``Configure the Network''.
        Select the \verb|eno3| interface.

        \item Set the domain name to \verb|eecs.utk.edu|.

        \item Generate a root password and keep it somewhere secure.

        \item Set up a user.

        \item Partition Disks:
            \begin{itemize}
                \item Select ``Manual''.
                \item Clear all existing partitions.
                \item Create an EFI partition:
                \begin{itemize}
                    \item Make this partition \verb|10 GB| at the beginning of the disk.
                    \item (We use a larger EFI partition to store custom kernels.)
                \end{itemize}
                \item Either create on partition for the root filesystem (\verb|/|), or create one for root and one for \verb|/home|.
                      This is up to you and you may choose to put one of those partitions on a different drive if one is available.
            \end{itemize}

        \item Continue through the installation until you get to ``Software selection''.
              Select only \verb|SSH server| and \\* \verb|standard system utilities|.

        \item Complete the installation and boot into the new system.
    \end{itemize}

\section{Users and Groups}
    \begin{itemize}
        \item Log in as \verb|root|.
        \item Install some basic software:
\begin{lstlisting}[language=bash]
root$ apt install build-essential git sudo vim
\end{lstlisting}
        \item Create users:
\begin{lstlisting}[language=bash]
root$ adduser ian
\end{lstlisting}
        \item Add users to the \verb|sudo| group:
\begin{lstlisting}[language=bash]
root$ usermod -aG sudo ian
\end{lstlisting}

    \end{itemize}

\section{Custom Kernel}
This process will install a custom kernel and enable eBPF tooling.

\begin{itemize}
    \item Install dependencies:
\begin{lstlisting}[language=bash]
ian$ sudo apt install autoconf bc bison cpio fakeroot flex git kmod libelf-dev libncurses-dev libssl-dev xz-utils rsync dwarves
\end{lstlisting}
    \item Clone the custom kernel:
\begin{lstlisting}[language=bash]
ian$ git clone https://gitlab.com/corsys/corsys-linux
\end{lstlisting}
    \item Check out a version:
\begin{lstlisting}[language=bash]
ian$ cd corsys-linux && git checkout corsys-5.14
\end{lstlisting}
    \item Check/edit the config file:
\begin{lstlisting}[language=bash]
ian$ vim .config
\end{lstlisting}
    Ensure these options are set to \verb|y| for BPF:
    \begin{itemize}
        \item \verb|CONFIG_BPF|
        \item \verb|CONFIG_BPF_SYSCALL|
        \item \verb|CONFIG_BPF_JIT|
        \item \verb|CONFIG_BPF_EVENTS|
        \item \verb|CONFIG_DEBUG_INFO|
        \item \verb|CONFIG_DEBUG_INFO_BTF|

    \end{itemize}
    Custom lab kernel options (set to \verb|y| as needed):
    \begin{itemize}
        \item \verb|CONFIG_CGROUP_PER_NODE_MAX|: limits the capacity of data that can be placed in the upper tier of memory for processes in a cgroup.
        \item \verb|CONFIG_MADV_COMPRESS|: adds an option to \verb|madvise| that instructs the kernel to compress the memory range.
        \item \verb|CONFIG_OBJECT_MAP|: allows access to information about arbitrary page ranges in a simple \verb|proc| file interface.
    \end{itemize}
    \item Build and install the kernel:
\begin{lstlisting}[language=bash]
ian$ make -j$(nproc) 2>&1 > out.txt # check for errors
ian$ sudo make modules_install
ian$ sudo make headers_install
ian$ sudo make install
ian$ sudo make -j $(nproc) -C tools/ perf_install prefix=/usr/
\end{lstlisting}
    \item Reboot (you may need to select the new kernel in the \verb|grub| menu)
\begin{lstlisting}[language=bash]
ian$ sudo reboot
\end{lstlisting}
\end{itemize}

\section{Essential Software}
    \begin{itemize}
        \item Replace \verb|mawk| with \verb|gawk|
\begin{lstlisting}[language=bash]
ian$ sudo apt install gawk
\end{lstlisting}
        \item Install \verb|perfmon|
\begin{lstlisting}[language=bash]
ian$ sudo apt install libpfm4-dev
\end{lstlisting}
        \item LLVM and Clang
\begin{lstlisting}[language=bash]
ian$ mkdir -p ~/src && cd ~/src
ian$ git clone https://github.com/llvm/llvm-project llvm && cd llvm
ian$ # optionally, check out a version
ian$ git checkout release/13.x
ian$ mkdir build && cd build
ian$ cmake ../llvm -DLLVM_ENABLE_PROJECTS='clang' -DCMAKE_BUILD_TYPE='RelWithDebInfo'
ian$ make -j$(nproc) && sudo make install
\end{lstlisting}
    \end{itemize}

\section{AEP NUMA Mode}
This describes how to set up the Intel Optane PMM as a separate numa device, a.k.a. AppDirect mode.
    \begin{itemize}
        \item Install \verb|ndctl| and \verb|ipmctl|:
\begin{lstlisting}[language=bash]
ian$ mkdir -p ~/src && cd ~/src
ian$ git clone https://github.com/pmem/ndctl && cd ndctl
ian$ sudo apt install autogen libnuma-dev numactl libtool libkmod-dev libudev-dev uuid-dev libjson-c-dev libkeyutils-dev
ian$ ./autogen.sh && ./configure
ian$ make -j$(nproc) && sudo make install
ian$ git clone https://github.com/intel/ipmctl && cd ipmctl
ian$ mkdir build && cd build
ian$ cmake .. -DCMAKE_INSTALL_PREFIX=/usr
ian$ make -j$(nproc) && sudo make install
\end{lstlisting}
        \item Put all PMMs on socket 0 into AppDirect mode:
\begin{lstlisting}[language=bash]
ian$ sudo ipmctl create -goal -socket 0 PersistentMemoryType=AppDirect
\end{lstlisting}
        \item Reboot.
        \item Migrate the device model:
\begin{lstlisting}[language=bash]
ian$ sudo daxctl migrate-device-model
\end{lstlisting}
        \item Reboot.
        \item Create a namespace for the memory region:
\begin{lstlisting}[language=bash]
ian$ sudo ndctl create-namespace --region region0 -m dax
\end{lstlisting}
        \item Copy module config file:
\begin{lstlisting}[language=bash]
ian$ sudo cp /usr/share/daxctl/daxctl.conf /etc/modprobe.d
\end{lstlisting}
        \item Reboot.
        \item Bind the namespace to NUMA:
\begin{lstlisting}[language=bash]
ian$ sudo daxctl reconfigure-device --no-movable --mode=system-ram all
\end{lstlisting}
        \item Check for correctness:
\begin{lstlisting}[language=bash]
ian$ numastat -m
\end{lstlisting}

    \end{itemize}


\section{Miscellaneous Tips}
    \begin{description}
        \item[\hspace{0.875cm}Disable Transparent Hugepages:]
~
\begin{lstlisting}[language=bash]
ian$ echo "never" | sudo tee /sys/kernel/mm/transparent_hugepage/enabled
\end{lstlisting}

        \item[\hspace{0.875cm}Use \texttt{CONFIG\_CGROUP\_PER\_NODE\_MAX} to limit PID \texttt{12345} to 20GB in the upper tier:]
~
\begin{lstlisting}[language=bash]
ian$ echo "+cpuset" | sudo tee /sys/fs/cgroup/cgroup.subtree_control
ian$ sudo rmdir /sys/fs/cgroup/1
ian$ sudo mkdir /sys/fs/cgroup/1
ian$ echo "21474836480" | sudo tee /sys/fs/cgroup/1/memory.node0_max
ian$ echo "12345" | sudo tee /sys/fs/cgroup/1/cgroup.procs
\end{lstlisting}
    \end{description}

\newpage

\section{Optional Software}
    \begin{itemize}
        \item \verb|zsh|
\begin{lstlisting}[language=bash]
ian$ sudo apt install zsh
ian$ chsh -s $(which zsh) ian
\end{lstlisting}
        \item \verb|tmux| and \verb|htop|
\begin{lstlisting}[language=bash]
ian$ sudo apt install tmux htop
\end{lstlisting}
        \item \verb|yed|
\begin{lstlisting}[language=bash]
ian$ git clone https://github.com/kammerdienerb/yed && cd yed
ian$ sudo ./install.sh
\end{lstlisting}
        \item \verb|mosh|
\begin{lstlisting}[language=bash]
ian$ mkdir -p ~/src && cd ~/src
ian$ git clone https://github.com/mobile-shell/mosh && cd mosh
ian$ sudo apt install protobuf-compiler libprotobuf-dev libncurses-dev libssl-dev autogen pkg-config
ian$ make -j$(nproc) && sudo make install
\end{lstlisting}
    \end{itemize}

\end{document}
